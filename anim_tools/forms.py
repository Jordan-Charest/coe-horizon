from django import forms

from bank_account.models import Transaction
from character.models import Character, Skill
from geology.models import Pole
from conditions.models import AppliedCondition
from program.models import InstalledProgram


class SelectCharacterForm(forms.Form):
    character = forms.ModelChoiceField(queryset=Character.objects.all())

class SelectPoleForm(forms.Form):
    pole = forms.ModelChoiceField(queryset=Pole.objects.all())


class SendMailForm(forms.Form):
    sender = forms.ModelChoiceField(queryset=Character.objects.all(), label='Envoyé Par')

    send_to_all = forms.BooleanField(label='Envoyer à tous', required=False)
    send_to_alpha = forms.BooleanField(label='Envoyer à Alpha-Solaris', required=False)
    send_to_atlas = forms.BooleanField(label='Envoyer à Altas', required=False)
    send_to_instavie = forms.BooleanField(label='Envoyer à Insta-vie', required=False)
    send_to_ascension = forms.BooleanField(label='Envoyer à Ascension', required=False)
    send_to_apex = forms.BooleanField(label='Envoyer à Apex', required=False)
    send_to_goodman = forms.BooleanField(label='Envoyer à Goodman', required=False)
    send_to_nephilim = forms.BooleanField(label='Envoyer à Nephilim', required=False)
    send_to_kanopi = forms.BooleanField(label='Envoyer à Kanopi', required=False)
    send_to_chrysalide = forms.BooleanField(label='Envoyer à Chrysalide', required=False)

    send_to_specific = forms.ModelMultipleChoiceField(queryset=Character.objects.filter(deactivated=False), label='Destinataire Spécifique', required=False)

    send_to_skill_holder = forms.ModelMultipleChoiceField(queryset=Skill.objects.all(), label='Envoyer aux destinataires ayant la compétence', required=False)

    use_cci = forms.BooleanField(label='Destinataires en CCI', initial=True, required=False)

    subject = forms.CharField(max_length=200, label='Sujet')
    content = forms.CharField(widget=forms.Textarea, label='Contenu')

class BankTransferForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ['amount', 'source', 'destination']

class CreateGenericAccountForm(forms.Form):
    account_name = forms.CharField(max_length=100)

class InstalledProgramForm(forms.ModelForm):
    class Meta:
        model = InstalledProgram
        fields = ['target', 'installed_by', 'program']

class AppliedConditionForm(forms.ModelForm):
    class Meta:
        model = AppliedCondition
        fields = ['condition', 'character']
