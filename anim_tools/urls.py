from django.contrib.auth import views as auth_views
from django.urls import path

from anim_tools import views

urlpatterns = [
    path('anim_overview/', views.anim_overview, name='anim_overview'),
    path('all_player_cards/', views.all_player_cards, name='all_player_cards'),
    path('install_program/', views.install_program, name='install_program'),
    path('apply_condition/', views.apply_condition, name='apply_condition'),
    path('specific_player_card/', views.specific_player_card, name='specific_player_card'),
    path('get_pole_qr/', views.get_pole_qr, name='get_pole_qr'),
    path('specific_player_card/', views.specific_player_card, name='specific_player_card'),
    path('anim_mail/', views.anim_mail, name='anim_mail'),
    path('anim_create_generic_account/', views.anim_create_generic_account, name='anim_create_generic_account'),
    path('anim_bank_transfer/', views.anim_bank_transfer, name='anim_bank_transfer'),
    path('view_char_emails/', views.view_emails_of, name='view_char_emails'),
    path('install_pole/', views.install_pole, name='install_pole'),
    path('list_poles/', views.list_poles, name='list_poles'),
    path('collect_pole/<pole_id>', views.collect_pole, name='collect_pole'),
]
