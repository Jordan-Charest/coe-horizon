from django.db import models


# Create your models here.
class BankAccount(models.Model):
    balance = models.IntegerField(default=0)
    investment_balance = models.IntegerField(default=0)

    def __str__(self):
        try:
            return self.character.__str__() + '(Compte de banque)'
        except:
            return 'Compte non associé'


class Transaction(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField()
    source = models.ForeignKey(BankAccount, on_delete=models.PROTECT, related_name='source')
    destination = models.ForeignKey(BankAccount, on_delete=models.PROTECT, related_name='destination')

    def __str__(self):
        return self.datetime.__str__() + ": " + self.source.character.__str__() + " sends " + self.amount.__str__() + " to " + self.destination.character.__str__()


