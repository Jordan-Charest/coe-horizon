from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from downtime.models import Sector
from character.models import SkillBought
from bank_account.models import Transaction, BankAccount


@receiver(pre_save, sender=Transaction)
def check_transaction_ok(sender, instance, raw, using, update_fields, **kwargs):
    finance_sector = Sector.objects.get(name="Finance")
    controlling_finance = finance_sector.controlling_character

    max_amount = instance.source.balance
    if controlling_finance and instance.source.character.corporation == controlling_finance.corporation:
        character = instance.source.character
        base_salary = character.grade.base_salary
        financement_bought = SkillBought.objects.filter(character=character, skill=financement)
        base_salary = base_salary + (financement_bought.count() * 500)

        max_amount = -base_salary

    if instance.source == instance.destination:
        raise Exception('Transfert vers sois-même impossible')

    if max_amount >= instance.amount:
        instance.source.balance = instance.source.balance - instance.amount
        instance.source.save()

        instance.destination.balance = instance.destination.balance + instance.amount
        instance.destination.save()
    else:
        raise Exception('Fonds inssufisants.')


@receiver(pre_save, sender=BankAccount)
def ensure_npc_account_has_appropriate_balance(sender, instance, raw, using, update_fields, **kwargs):
    try:
        character = instance.character
        if character.player.is_staff:
            mult = 3

            if character.corporation.name == "Nephilim Trust":
                mult = 6
            mult += SkillBought.objects.filter(character=character, skill__name='Financement').count()

            target_balance = character.grade.base_salary * mult
            if instance.balance < target_balance:
                instance.balance = target_balance

    except Exception:
        # parent char was probably deleted, everything is fine (insert fire meme here)
        pass
