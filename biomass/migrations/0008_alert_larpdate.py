# Generated by Django 2.2.4 on 2019-08-22 23:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('larpdate', '0002_larpdate_current'),
        ('biomass', '0007_auto_20190822_1936'),
    ]

    operations = [
        migrations.AddField(
            model_name='alert',
            name='larpdate',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='larpdate.LarpDate'),
        ),
    ]
