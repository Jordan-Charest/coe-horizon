import logging
from datetime import datetime

import numpy as np
from django.db import models
from django.utils import timezone

from character.models import Character
from character.utils import get_generic_account
from extra_mail.models import Message
from larpdate.models import LarpDate


# Create your models here.
class BiomassType(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    prefered_food = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.name

class AlertType(models.Model):
    weight = models.IntegerField()
    name = models.CharField(max_length=70)
    description = models.TextField()
    type_blacklist = models.ManyToManyField(BiomassType, blank=True)

    def __str__(self):
        return f'{self.name} (Default weight: {self.weight})'

class Alert(models.Model):
    alert_type = models.ForeignKey(AlertType, on_delete=models.PROTECT)
    execute_at = models.DateTimeField(default=None, blank=True, null=True)
    expires_at = models.DateTimeField(default=None, blank=True, null=True)
    biomass = models.ForeignKey('Biomass', on_delete=models.CASCADE)

    started = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    success = models.BooleanField(default=False)

    larpdate = models.ForeignKey(LarpDate, on_delete=models.PROTECT, null=True)

    def get_target_hours(self):
        target_hours = 2

        if self.biomass.biomass_type.name.lower() == 'procaryotes':
            target_hours = 3

        return target_hours

    @property
    def description(self):
        desc = self.alert_type.description
        desc = desc.replace('BIOMASS_NAME', f'la biomasse "{self.biomass.biomass_type.name}"')
        desc = desc.replace('BIOMASS_TYPE', self.biomass.biomass_type.prefered_food)

        return desc

    def fail(self):
        self.completed = True
        self.success = False
        self.save()

        self.biomass.generate_alert()

    def succeeded(self):
        loot_count = np.random.choice(range(3,6),1)[0]
        message_content = f'Votre biomasse "{self.biomass.biomass_type.name}" à produit {loot_count} biomasses filles!'

        account = get_generic_account('Sonde Biomasse')

        message = Message(
            by=account,
            subject=f'Nouvelle biomasse-fille détecté',
            content=message_content
        )
        message.save()
        message.to.add(self.biomass.owner)
        message.save()


        self.completed = True
        self.success = True
        self.save()

        self.biomass.generate_alert()



    def execute(self):
        message_content = 'Une alerte à été émise pour une de vos Biomasses.'

        account = get_generic_account('Sonde Biomasse')

        message = Message(
            by=account,
            subject=f'Alerte Biomasse: {self.alert_type.name}',
            content=message_content
        )

        message.save()
        message.to.add(self.biomass.owner)
        message.save()

        self.started=True
        self.save()


class Biomass(models.Model):
    owner = models.ForeignKey(Character, on_delete=models.CASCADE)
    biomass_type = models.ForeignKey(BiomassType, on_delete=models.PROTECT)
    next_alert = models.ForeignKey(Alert, on_delete=models.CASCADE, related_name='next_alert', null=True, blank=True)

    alive = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.owner}: {self.biomass_type}'

    @property
    def status(self):
        last_two_alerts = Alert.objects.filter(completed=True, biomass=self).order_by('-id')[:2]
        count_failed = 0
        for alert in last_two_alerts:
            if not alert.success:
                count_failed = count_failed + 1

        if count_failed == 2:
            return 'Détruite'
        elif count_failed == 1:
            return 'Instable'
        elif count_failed == 0:
            return 'Stable'

    @property
    def has_active_alert(self):
        now = timezone.now()

        active_alerts = Alert.objects.filter(
            biomass=self,
            execute_at__lt=timezone.now(),
            expires_at__gt=timezone.now(),
            completed=False
        )

        return active_alerts.count() > 0

    def generate_alert(self, alert_now=False):
        if self.status == 'Détruite':
            self.alive = False
            self.save()
        else:
            larpdate = LarpDate.objects.get(current=True)
            now = datetime.now()
            delta = now.date() - larpdate.out_date

            if delta.days == 0 and now.hour < 20:
                from biomass.utils import get_alert_type
                alert_type = get_alert_type(self)
                alert = Alert(alert_type=alert_type, biomass=self, larpdate=larpdate)
                if alert_now:
                    alert.execute_at = now

                alert.save()
                self.next_alert = alert
                self.save()
