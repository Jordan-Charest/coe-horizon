from datetime import datetime, timedelta

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from biomass.models import Alert
from biomass.utils import get_next_run
from larpdate.models import LarpDate


@receiver(post_save, sender=Alert)
def ensure_alert_has_execute_time(sender, instance, created, raw, using, update_fields, **kwargs):
    now = datetime.now()

    if instance.execute_at is None:
        # First initialization
        target_hours = instance.get_target_hours()
        target_datetime = now + timedelta(hours=target_hours)
        execute_at = get_next_run(target_datetime)

        instance.execute_at = execute_at
        instance.save()

    # Ensure expiration is 3 hours after execute_at
    if instance.expires_at is None:
        instance.expires_at = instance.execute_at + timedelta(hours=3)
        instance.save()
