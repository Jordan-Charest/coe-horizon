from datetime import datetime, timedelta

import numpy as np
from django.utils import timezone

from biomass.models import Alert, AlertType
from larpdate.models import LarpDate


def get_next_run(target_datetime):
    # Standard Deviation, 30 minutes
    scale = 20 * 60

    loc = target_datetime.timestamp()

    distribution = np.random.normal(loc=loc, size=1, scale=scale)

    return timezone.make_aware(datetime.fromtimestamp(distribution[0]))

def get_alert_type(biomass):
    alerts = AlertType.objects.all()
    clarp = LarpDate.objects.get(current=True)
    plateau_croissance = AlertType.objects.get(name='Plateau de croissance')

    alert_t = []
    alert_w = []
    for alert in alerts:
        alert_t.append(alert)
        # "Plateau de croissance" does not ever become rarer
        if alert != plateau_croissance:
            alert_w.append(float(alert.weight + biomass.alert_set.filter(alert_type=alert, larpdate=clarp).count()))
        else:
            alert_w.append(float(alert.weight))

    # invert weights
    alert_w = np.reciprocal(alert_w)
    # normalize
    alert_w = alert_w / np.sum(alert_w)

    return np.random.choice(alert_t, 1, p=alert_w)[0]

def run_alerts():
    # retreive all alerts that are due
    now = datetime.now()
    due_alerts = Alert.objects.filter(started=False, execute_at__lt=now)

    for alert in due_alerts:
        alert.execute()

    failed_alerts = Alert.objects.filter(started=True, completed=False, expires_at__lt=now)

    for alert in failed_alerts:
        alert.fail()
