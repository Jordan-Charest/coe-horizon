from django.core.management.base import BaseCommand
from character.models import Character
from character.utils import char_has_skill


def init_alter_egos():
    player_chars = Character.objects.filter(player__is_staff=False, deactivated=False)
    for char in player_chars:
        if char_has_skill(char, 'Alter-Ego Virtuel') and len(player_chars) > 1:
            # Assumes current character is the original one as it has a skill
            remaining_chars = player_chars.filter(player=char.player).exclude(id=char.id)
            for alter_char in remaining_chars:
                alter_char.is_alterego = True
                alter_char.parent_char = char
                alter_char.save()


class Command(BaseCommand):
    help = 'Parses Horizon website and puts result in skills'

    def handle(self, *args, **kwargs):
        init_alter_egos()
