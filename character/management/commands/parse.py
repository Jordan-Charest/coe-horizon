import pprint
import re

import requests
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand

from character.models import Skill

pp = pprint.PrettyPrinter()

def parse_page(link):
    content = requests.get(link).content
    soup = BeautifulSoup(content, 'html5lib')

    skill_sections = soup.findAll('div', {'class': 'module_text_area'})

    skills = []
    for section in skill_sections[1:]:
        skills += section.__str__().split('<h6 style')

    parsed = []

    for skill in skills:
        skill_header_string = '<span style="color: #00a8ff;">'
        if skill_header_string in skill:
            csoup = BeautifulSoup(skill, 'html5lib')
            name_and_cost = csoup.findAll('span')[0].__str__().split(' - ')
            if len(name_and_cost) > 2:
                name = name_and_cost[0]
                for n in name_and_cost[1:len(name_and_cost)-1]:
                    name += ' - ' + n
                cost = name_and_cost[len(name_and_cost)-1]

                name_and_cost = [name, cost]

            name = strip_tags(name_and_cost[0]).strip()
            cost = strip_tags(name_and_cost[1]).strip()
            cost = re.sub('Exp', '', cost).strip()
            desc_match = re.search('</h6>(.*)', skill, re.MULTILINE)


            desc = strip_tags(desc_match.group(1))


            try:
                desc_short_match = re.search('</h6>(.*)Liste(.*)', skill, re.MULTILINE)
                desc_short = strip_tags(desc_short_match.group(1))
            except:
                desc_short = desc
            
            specialities = re.search("(<details.*)", skill, re.MULTILINE)
            spec_parent_char = "➤"
            spec_child_char = "∙"

            spec_list = []

            if specialities:
                stripped = strip_tags(specialities.group(1))
                parents = stripped.split(spec_parent_char)[1:]
                if any('∙' in p for p in parents):
                    for parent in parents:
                        prefix = parent.split(spec_child_char)[0].strip()
                        suffixes = parent.split(spec_child_char)[1:]
                        for suffix in suffixes:
                            spec_list.append(name + ' (' + prefix + ' - ' + suffix + ')')
                else:
                    for parent in parents:
                        spec_list.append(name + ' (' + prefix + ')')
                    spec_list = parents

            
            if len(spec_list) > 0:
                for spec in spec_list:
                    parsed.extend(parse_item(spec, cost, desc_short))
            else:
                parsed.extend(parse_item(name, cost, desc_short))

    return parsed

def parse_item(name, cost, desc):
    parsed = []

    try:
        parsed.append({
            'name': name,
            'cost': int(cost),
            'description': desc
        })
    except:
        costs = cost.split('/')
        costs = list(set(costs))

        for c in costs:
            try:
                parsed.append({
                    'name': name,
                    'cost': int(c),
                    'description': desc
                })
            except:
                pass

    return parsed


def parse_site(silent=False):
    locations = [
        'http://horizon.coegn.org/site/competences-generales/',
        'http://horizon.coegn.org/site/competences-influence/',
        'http://horizon.coegn.org/site/competences-martiales/',
        'http://horizon.coegn.org/site/competences-informatiques/',
        'http://horizon.coegn.org/site/competences-techniques/',
        'http://horizon.coegn.org/site/competences-biomedicales/',
    ]

    parsed = []
    for l in locations:
        parsed = parse_page(l) + parsed
        if not silent:
            print(len(parsed))

    return parsed

def strip_tags(text):
    return re.sub('<[^<]+?>', '', text)

def run(ask=True):
    skills = Skill.objects.all()
    parsed = parse_site(silent=not ask)

    for p_skill in parsed:

        fskills = skills.filter(name=p_skill['name'])


        if fskills.count() == 0 or ask == False:
            # Nothing exists, create anew
            new_skill = Skill(
                name=p_skill['name'],
                xp_cost=p_skill['cost'],
                description=p_skill['description'],
                has_contact_skill=False
            )
            new_skill.save()
        elif (fskills.count() == 1 and fskills.first().xp_cost == p_skill['cost']) or fskills.filter(xp_cost=p_skill['cost']).count() == 1:
            # Only one skill with name and XP cost exists, update with new description
            fskills = fskills.filter(xp_cost=p_skill['cost'])
            new_skill = fskills.first()
            new_skill.description = p_skill['description']
            new_skill.save()
        else:
            print('')
            print('')
            print('')
            print('The following duplicates where found: ')
            looped_skills = []

            i=0
            for skill in fskills:
                print(str(i) + ' - ' + skill.__str__())
                looped_skills.append(skill)
                i += 1

            print('You are currently operating on the following skill: ' + p_skill['name'] + ' - ' + str(p_skill['cost']) + ' XP')
            try:
                result = input('Please enter the index of the skill to overwrite, n for a new entry or s to skip: ')
                if result == 'n':
                    new_skill = Skill(
                        name=p_skill['name'],
                        xp_cost=p_skill['cost'],
                        description=p_skill['description'],
                        has_contact_skill=False
                    )
                    new_skill.save()
                else:
                    result = int(result)
                    new_skill = looped_skills[result]
                    new_skill.xp_cost = p_skill['cost']
                    new_skill.description = p_skill['description']
                    new_skill.save()
            except:
                print('skipped')


def update_contact_skills():
    # Marché noir, Contact corporatif, Contact illicite, collaboration clandestine et Relations corporatifs:
    skill_prefix_to_update = [
        'Fournisseurs',
        'Marché noir',
        'Collaboration clandestine',
        'Relations corporatives',
        'Contact'
    ]

    for prefix in skill_prefix_to_update:
        to_update = Skill.objects.all().filter(name__startswith=prefix)
        for iskill in to_update:
            if not iskill.has_contact_skill:
                print("Updating: " + iskill.name)
                iskill.has_contact_skill = True
                iskill.save()

class Command(BaseCommand):
    help = 'Parses Horizon website and puts result in skills'

    def handle(self, *args, **kwargs):
        run()
        # update_contact_skills()
