import numpy as np
from django.core.management.base import BaseCommand

from bank_account.models import Transaction
from biomass.models import Biomass, BiomassType
from character.models import Character, Skill, SkillBought
from character.utils import get_generic_account
from program.models import InstalledProgram

from downtime.utils import send_answers


def pay_everyone():
    # Get or create pay character
    pay = get_generic_account('Paye')

    source_account = pay.bank_account

    for character in Character.objects.all():
        if character != pay:
            source_account.balance = 9999999
            source_account.save()

            financement = Skill.objects.get(name='Financement')
            base_salary = character.grade.base_salary

            try:
                financement_bought = SkillBought.objects.filter(character=character, skill=financement)
                base_salary = base_salary + (financement_bought.count() * 500)
            except:
                # Character has not bought Financement
                pass


            if character.origin.name in ['Martien', "Dette d’embarquement"]:
                base_salary = round(base_salary * 0.8)

            pay_transaction = Transaction(amount=base_salary, source=source_account, destination=character.bank_account)
            pay_transaction.save()

def first_biomass_alert():
    # Ensure all Biologists have received their first mother
    biology = Skill.objects.get(name='Bioproducteur')
    biology_bought = SkillBought.objects.filter(skill=biology)

    for bio in biology_bought:
        character = bio.character

        if Biomass.objects.filter(owner=character, alive=True).count() == 0:
            biomass_types = BiomassType.objects.exclude(name="Procaryote")
            biomass_type = np.random.choice(biomass_types)

            biomass = Biomass(owner=character, biomass_type=biomass_type)
            biomass.save()


    # Ensure all Biomasses have an alert
    for biomass in Biomass.objects.filter(alive=True):
        biomass.generate_alert(alert_now=True)

def clear_firewalls():
    firewalls = InstalledProgram.objects.filter(
        program__id_code__icontains='PF'
    ).exclude(target__player__is_staff=True)
    for firewall in firewalls:
        firewall.delete()

class Command(BaseCommand):
    help = 'Provides Pay and start biomass timers when required'

    def handle(self, *args, **kwargs):
        # pay_everyone()
        first_biomass_alert()
        clear_firewalls()
        send_answers()
