from bank_account.models import BankAccount
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from larpdate.models import LarpDate


# Create your models here.
class Skill(models.Model):
    name = models.CharField(max_length=100)
    xp_cost = models.PositiveSmallIntegerField()
    description = models.TextField()
    has_contact_skill = models.BooleanField(default=False)
    can_be_bought_more_than_once = models.BooleanField(default=False)
    is_deprecated = models.BooleanField(default=False)

    @property
    def contact_name(self):
        if self.has_contact_skill:
            contact_name = "CONTACT: " + self.name
        else:
            contact_name = None

        return contact_name

    def __str__(self):
        return self.name + ' (' + self.xp_cost.__str__() + ' XP)'

    def __unicode__(self):
        return self.__str__()


class Corporation(models.Model):
    name = models.CharField(max_length=100)
    anim_only = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Origin(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()
    bonus = models.TextField()
    malus = models.TextField()

    def __str__(self):
        return self.name


class Grade(models.Model):
    name = models.CharField(max_length=100)
    base_salary = models.PositiveSmallIntegerField(default=500)

    def __str__(self):
        return self.name


class Character(models.Model):
    origin = models.ForeignKey(Origin, on_delete=models.PROTECT)
    corporation = models.ForeignKey(
        Corporation, null=True, blank=True, on_delete=models.PROTECT)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    player = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)

    security_token = models.CharField(max_length=70, null=True, blank=True)

    bonus_xp = models.PositiveSmallIntegerField(default=0)

    skills = models.ManyToManyField(Skill, through='SkillBought')
    level = models.PositiveSmallIntegerField(default=1)

    deactivated = models.BooleanField(default=False)
    encrypted_until = models.DateTimeField(null=True, blank=True)
    encrypted = models.BooleanField(default=False)
    compromised = models.BooleanField(default=False)
    selected = models.BooleanField(default=True)

    bank_account = models.OneToOneField(
        BankAccount, on_delete=models.PROTECT, null=True, blank=True)

    birth_place = models.CharField(max_length=70, null=True, blank=True)
    age = models.PositiveSmallIntegerField(default=0)

    notes = models.TextField(null=True, blank=True)
    loot = models.TextField(null=True, blank=True)

    grade = models.ForeignKey(
        Grade, on_delete=models.PROTECT, null=True, blank=True)

    current_influence = models.IntegerField(default=0)
    passive_innovation = models.IntegerField(default=0)
    current_larp_active = models.BooleanField(default=False)
    paid_larps = models.ManyToManyField(LarpDate, related_name='paid_larps', blank=True)

    is_alterego = models.BooleanField(default=False)
    parent_char = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True)

    @property
    def remaining_downtimes(self):
        try:
            from downtime.models import Action, Tick, Assistance
            tick = Tick.objects.get(completed=False)

            char_actions = Action.objects.filter(tick=tick, character=self)
            char_assistances = Assistance.objects.filter(
                action__tick=tick, character=self)

            used_actions = [action.uses_downtime_action for action in char_actions].count(
                True) + [assistance.uses_downtime_action for assistance in char_assistances].count(True)

            return 2 - used_actions
        except:
            return 0

    @property
    def effective_influence(self):
        try:
            from downtime.models import Action, Assistance, Tick
            tick = Tick.objects.get(completed=False)
            current_actions = Action.objects.filter(tick=tick).exclude(action_type=Action.SECURITY_SYSTEM)

            character_actions = current_actions.filter(character=self)
            influence_spent_actions = character_actions.aggregate(
                Sum('influence_invested'))['influence_invested__sum']
            if influence_spent_actions is None:
                influence_spent_actions = 0

            current_assistance = Assistance.objects.filter(action__tick=tick)
            character_assistances = current_assistance.filter(character=self)
            influence_spent_assistances = character_assistances.aggregate(
                Sum('influence_invested'))['influence_invested__sum']
            if influence_spent_assistances is None:
                influence_spent_assistances = 0

            return self.current_influence - influence_spent_actions - influence_spent_assistances
        except:
            return self.current_influence

    def spend_influence(self):
        from downtime.models import Action, Assistance, Tick
        tick = Tick.objects.get(calc_done=False)
        current_actions = Action.objects.filter(tick=tick)

        character_actions = current_actions.filter(character=self)
        influence_spent_actions = character_actions.aggregate(
            Sum('influence_invested'))['influence_invested__sum']
        if influence_spent_actions is None:
            influence_spent_actions = 0

        current_assistance = Assistance.objects.filter(action__tick=tick)
        character_assistances = current_assistance.filter(character=self)
        influence_spent_assistances = character_assistances.aggregate(
            Sum('influence_invested'))['influence_invested__sum']
        if influence_spent_assistances is None:
            influence_spent_assistances = 0

        self.current_influence =  self.current_influence - influence_spent_actions - influence_spent_assistances
        self.save()

    def gen_influence(self):
        from downtime.models import Action, Assistance, Tick, Sector
        tick = Tick.objects.get(calc_done=False)
        current_actions = Action.objects.filter(tick=tick,
                                                action_type=Action.GENERATION)

        character_actions = current_actions.filter(character=self)

        industrie_lourde = Sector.objects.get(name="Industrie lourde")

        influence_to_add = 0
        for action in character_actions:
            if industrie_lourde.controlling_character.corporation == self.corporation and char_has_skill(self, 'Ingénierie spécialisée - Innovation'):
                influence_to_add += 2
            else:
                influence_to_add += 1

        self.current_influence += influence_to_add
        self.save()


    @property
    def is_locked(self):
        return self.encrypted or self.compromised

    @property
    def total_xp(self):
        if self.is_alterego:
            return 0

        origin_bonus = 0
        if self.origin.name == 'Humain - Non-affilié':
            origin_bonus += 1
        return 25 + ((self.level-1) * 2) + self.bonus_xp + origin_bonus

    @property
    def spent_xp(self):
        spent = 0

        sb = SkillBought.objects.filter(character=self)
        for isb in sb:
            spent += isb.effective_cost

        return spent

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name

    @property
    def current_xp(self):
        return self.total_xp - self.spent_xp

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def __unicode__(self):
        return self.__str__()


class SkillBought(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.PROTECT)
    count = models.PositiveSmallIntegerField(default=1)
    cost_override = models.PositiveSmallIntegerField(
        default=None, blank=True, null=True)

    def __str__(self):
        if self.cost_override is not None:
            return self.character.__str__() + ' - ' + self.skill.name + ' (' + self.cost_override.__str__() + ' XP)'
        else:
            return self.character.__str__() + ' - ' + self.skill.__str__()

    @property
    def clean_str(self):
        if self.cost_override is not None:
            return self.skill.name + ' (' + self.cost_override.__str__() + ' XP)'
        else:
            return self.skill.__str__()

    @property
    def effective_cost(self):
        if self.cost_override is None:
            return self.skill.xp_cost
        else:
            return self.cost_override

    @property
    def searchname(self):
        return self.__str__()
