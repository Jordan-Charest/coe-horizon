var detected_cams = [];
let scanner_block = $("#scanner_section");
scanner_block.append("<ul id='camlist'></ul>");
let cam_list = $('#camlist');

video = document.getElementById('preview');
video.setAttribute('autoplay', '');

let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror:false }, );
scanner.addListener('scan', function (content) {
    content = content.split("'").join('"');
    let parsed = JSON.parse(content);
    process_scan(parsed);
});

Instascan.Camera.getCameras().then(function (cameras) {
  if (cameras.length > 0) {
      scanner.start(cameras[0]);

      jQuery.each(cameras, function(i,camera) {
          if (camera.name == null) {
              camera.name = 'Nom Inconnu';
          }
          cam_list.append("<li camid=" + i + " class=\"camera_selector\">" + camera.name + "</li>");
          detected_cams.push(camera);
      });

      $('.camera_selector').on('click', function () {
          i = event.target.attributes['camid'].value;
          scanner.stop();
          scanner.start(detected_cams[i]);
      });
  } else {
    console.error('No cameras found.');
  }
}).catch(function (e) {
  console.error(e);
});

