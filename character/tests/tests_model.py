from django.contrib.auth.models import User
from django.test import TestCase

from character.models import Character, Origin, Skill


# Create your tests here.
class CharacterTestCase(TestCase):
    def setUp(self):
        self.player = User()
        self.player.save()

        self.skill1 = Skill(xp_cost=1)
        self.skill2 = Skill(xp_cost=5)
        self.skill1.save()
        self.skill2.save()

        self.origin = Origin()
        self.origin.save()

        self.character = Character(player=self.player, origin=self.origin)
        self.character.save()

    def test_total_xp_is_correct(self):
        self.assertEqual(self.character.total_xp, 25)

        self.character.level = 9
        self.character.save()

        self.assertEqual(self.character.total_xp, 41)

    def test_bonus_xp_is_calculated(self):
        self.character.bonus_xp = 5
        self.character.save()
        self.assertEqual(self.character.total_xp, 30)
