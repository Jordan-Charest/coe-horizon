from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('create_account/', views.create_account, name='create_account'),
    path('create_character/<alter_ego_id>', views.create_character, name='create_character'),
    path('sheet/<character_id>', views.character_sheet, name='sheet'),
    path('buy_skill/<character_id>', views.buy_skill, name='buy_skill'),
    path('login/', views.view_login, name='character_login'),
    path('list_characters/', views.list_characters, name='list_characters'),
    path('access_card/', views.access_card, name='access_card'),
    path('compromised/', views.account_compromised, name='account_compromised')
]

auth_urls = [
    path(r'password_reset/',
         auth_views.PasswordResetView.as_view(
             template_name='registration/password_reset_form.djhtml',
             email_template_name='registration/password_reset_email.djhtml',
             from_email='admin@coegn.org',
             subject_template_name='registration/reset_subject.txt'
         ),
         name='password_reset'),
    path(r'password_reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.djhtml'),
         name='password_reset_done'
        ),
    path(r'reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.djhtml'),
         name='password_reset_confirm'),
    path(r'reset/done/',
         auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.djhtml'),
         name='password_reset_complete'),
    path('logout',
         auth_views.logout_then_login,
         name='logout')
]

urlpatterns += auth_urls
