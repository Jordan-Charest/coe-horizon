from django.contrib import admin

from craft.models import CraftRecord, Recipe, RecipeCategory

# Register your models here.
admin.site.register(Recipe)
admin.site.register(RecipeCategory)
admin.site.register(CraftRecord)
