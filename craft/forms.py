from django import forms

from craft.models import CraftRecord, Recipe
from bank_account.models import Transaction
from character.utils import char_has_skill, get_generic_account
from larpdate.utils import larp_is_active


def calc_recipe_final_cost(recipe, crafter_count, number_of_items):
    savings_by_crafter_count = {
        1: 0,
        2: 25,
        3: 50,
        4: 100,
    }

    if crafter_count > 4:
        crafter_count = 4

    craft_price = recipe.craft_price
    if craft_price is None:
        craft_price = 0

    floor = min(10, craft_price)

    final_price = craft_price - savings_by_crafter_count[crafter_count]
    final_price = max(final_price, floor)

    return final_price * number_of_items

def calc_recipe_final_time(recipe, crafter_count, number_of_items):
    """
    Same as calc_recipe_final_cost, but for crafting time
    """
    savings_by_crafter_count = {
        1: 0,
        2: 10,
        3: 20,
        4: 30,
    }

    if crafter_count > 4:
        crafter_count = 4

    craft_time = recipe.craft_time
    if craft_time is None:
        craft_time = 0

    floor = min(5, craft_time)

    final_time = craft_time - savings_by_crafter_count[crafter_count]
    final_time = max(final_time, floor)

    return final_time*number_of_items

def format_prereqs(prereq_str):
    """
    Takes a prereq_str in the form "prereq1 + prereq2 (2) + ... + prereqN" and returns a list
    in the form ["prereq1", ["prereq2", 2], ... , "prereqN"]

    if there is a multiple of an item, it returns the item name in a list with the second element being the number of items
    
    """
    if prereq_str is None:
        return []

    prereq_list = prereq_str.split(sep="+")

    formatted_prereq_list = []

    for i in range(len(prereq_list)):

        prereq_list[i] = prereq_list[i].strip()

        if prereq_list[i][-1] == ")" and prereq_list[i][-3] == "(": # Then a multiple of the item is needed
            item_name = prereq_list[i][:-3].strip()
            item_number = int(prereq_list[i][-2])
            formatted_prereq_list.append([item_name, item_number])
        else:
            formatted_prereq_list.append(prereq_list[i])

    return formatted_prereq_list

def get_recipe(recipe_str):
    """
    Returns a Recipe object when given the name of the recipe.
    """

    try:
        recipe = Recipe.objects.get(name=recipe_str)
    except:
        recipe = Recipe(name=recipe_str)

    return recipe

def recipe_list_of_prereqs(recipe, level=-1):
    """
    Returns list of sub-items needed to craft target recipe.

    level is the recursivity index. level=0 for top level prerequisites.

    The list "level_list" is used solely to indent the recipe names properly in the Django table.
    The list "number_list" is likewise used to format the table. It is a bit clunky, but it works. 
    Feel free to change if there is a better alternative!
    """
    
    level += 1

    list_of_resources = []
    level_list = []
    number_list = []

    list_prereqs = format_prereqs(recipe.resource_cost)

    if list_prereqs == []: # No prerequisites for item
        return [], [], []

    for i in range(len(list_prereqs)):
        if type(list_prereqs[i]) is list:
            list_prereqs[i][0] = get_recipe(list_prereqs[i][0])
        else:
            list_prereqs[i] = get_recipe(list_prereqs[i])

    for item in list_prereqs:
        if type(item) is list: # Then there are multiple of the item
            list_of_resources.append(item[0])
            level_list.append(level)
            number_list.append(item[1])

            resources_to_append, level_to_append, number_to_append = recipe_list_of_prereqs(item[0], level)

            list_of_resources += resources_to_append
            level_list += level_to_append
            number_list += number_to_append


        else:
            list_of_resources.append(item)
            level_list.append(level)
            number_list.append(1)

            resources_to_append, level_to_append, number_to_append = recipe_list_of_prereqs(item, level)

            list_of_resources += resources_to_append
            level_list += level_to_append
            number_list += number_to_append

    return list_of_resources, level_list, number_list

def return_cost_time_name(recipe, crafter_count, level, number):
    """
    Given a Recipe object, returns a tuple in the form

    (Recipe, cost, time, name)

    With cost and time adjusted for "Gestionnaire de projets" and name adjusted for indentation and number.
    """

    recipe_name_formatted = f"{'——'*level} {recipe.name} ({number})"

    if recipe.craft_price in (0, '-', None):
        if recipe.craft_time in (0, '-', None): # Then the item cannot be crafted
            return (recipe, f"{recipe.market_price}*", 0, recipe_name_formatted)
        else: # Then the item can be crafted but it is free to do so
            return (recipe, 0, calc_recipe_final_time(recipe, crafter_count, 1), recipe_name_formatted)

    final_cost = calc_recipe_final_cost(recipe, crafter_count, 1)
    final_time = calc_recipe_final_time(recipe, crafter_count, 1)

    return (recipe, final_cost, final_time, recipe_name_formatted)

### FORMS CLASSES ###

class CraftItemForm(forms.ModelForm):
    downtime = forms.BooleanField(
        initial=False,
        widget=forms.HiddenInput,
        required=False
    )

    class Meta:
        model = CraftRecord
        fields = ['recipe', 'number_of_crafters', 'number_of_items', 'crafter', 'extra_spend']
        labels = {
            'recipe': 'Objet',
            'number_of_crafters': 'Compétence "Gestion de projet": Nombre total de fabriquants',

            'extra_spend': '(Secteur Resources Naturelles) Crédits supplémentaires à utiliser:',
        }
        widgets = {
            'crafter': forms.HiddenInput(),
            'extra_spend': forms.HiddenInput(),
        }

    def clean(self):
        cleaned_data = super().clean()
        crafter = cleaned_data.get('crafter')
        recipe = cleaned_data.get('recipe')
        number_crafted = cleaned_data.get('number_of_items')

        if not larp_is_active() and not cleaned_data.get('downtime'):
            raise forms.ValidationError("L'activitée n'est pas actuellement en cours, veuillez réessayer pendant l'activité ou la session de downtime")

        if cleaned_data.get('downtime'):
            if number_crafted > recipe.max_per_downtime:
                raise forms.ValidationError(f'Cet objet peut-être fabriquer un maximum de {recipe.max_per_downtime} par action de Downtime!')

        prerequisite_skill = recipe.category.prerequisite_skill.name
        if not char_has_skill(crafter, prerequisite_skill):
            raise forms.ValidationError(f'Vous devez avoir la compétence "{prerequisite_skill}" pour fabriquer cet objet!')

        target = get_generic_account('Matériaux Divers')
        final_cost = calc_recipe_final_cost(recipe, cleaned_data.get('number_of_crafters'), number_crafted)

        transaction = Transaction(
            amount=final_cost + cleaned_data.get('extra_spend'),
            source=crafter.bank_account,
            destination=target.bank_account
        )

        try:
            transaction.save()
        except Exception:
            raise forms.ValidationError(
                f'Fonds inssufisants, au moins {final_cost}cr sont requis dans votre compte!'
            )

class CraftSimForm(forms.Form):

    recipe = forms.ModelChoiceField(label='Objet' , queryset=Recipe.objects.all())
    number_of_crafters = forms.IntegerField(label='Compétence "Gestion de projet" : Nombre total de fabriquants', initial=1, min_value=1)


    def clean(self):
        cleaned_data = super().clean()
        recipe = cleaned_data.get('recipe')

        resource_list, level_list, number_list = recipe_list_of_prereqs(recipe)

        level_list = [0] + level_list
        resource_list = [recipe] + resource_list
        number_list = [1] + number_list
        # All three of these lists are used to format the table

        resource_list = [return_cost_time_name(recipe, cleaned_data.get('number_of_crafters'), level_list[i], number_list[i]) for i, recipe in enumerate(resource_list)]

        self.resource_list = resource_list