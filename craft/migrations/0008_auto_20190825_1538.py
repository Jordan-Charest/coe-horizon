# Generated by Django 2.2.4 on 2019-08-25 19:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('craft', '0007_auto_20190825_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='craft.RecipeCategory'),
        ),
    ]
