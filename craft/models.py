from django.db import models

from character.models import Character, Skill


# Create your models here.
class RecipeCategory(models.Model):
    name = models.CharField(max_length=50)
    prerequisite_skill = models.ForeignKey(Skill, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(RecipeCategory, on_delete=models.PROTECT, null=True)

    identification = models.CharField(max_length=100, blank=True, null=True)
    slot = models.CharField(max_length=100, blank=True, null=True)
    prerequisite = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    defective_effect = models.TextField(blank=True, null=True)
    market_price = models.IntegerField(blank=True, null=True)
    craft_price = models.IntegerField(blank=True, null=True)
    resource_cost = models.TextField(blank=True, null=True)
    craft_time = models.IntegerField(blank=True, null=True)
    craft_steps = models.TextField(blank=True, null=True)
    downtime_cost = models.IntegerField(default=0, null=True)
    max_per_downtime = models.IntegerField(default=0, null=True)

    royalties_target = models.ForeignKey(Character, null=True, blank=True, on_delete=models.PROTECT)
    royalties_percent = models.PositiveSmallIntegerField(default=5)

    is_public = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class CraftRecord(models.Model):
    crafter = models.ForeignKey(Character, on_delete=models.CASCADE)
    recipe = models.ForeignKey(Recipe, on_delete=models.PROTECT)
    number_of_crafters = models.IntegerField(default=1)
    number_of_items = models.IntegerField(default=1)

    extra_spend = models.PositiveIntegerField(default=0)

    timestamp = models.DateTimeField(auto_now_add=True, null=True)

    @property
    def final_cost(self):
        from craft.forms import calc_recipe_final_cost
        return calc_recipe_final_cost(self.recipe, self.number_of_crafters, self.number_of_items)
