from django.contrib.auth import views as auth_views
from django.urls import path

from craft import views

urlpatterns = [
    path('craft_overview/', views.craft_overview, name='craft_overview'),
    path('craft_item/', views.craft_item, name='craft_item'),
    path('craft_simulator/', views.craft_simulator, name='craft_simulator'),
]
