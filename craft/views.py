from django.shortcuts import redirect, render
from django import forms

from character import views as char_views
from character.utils import get_character
from downtime.utils import sector_controlled_by_corp
from craft.forms import CraftItemForm
from craft.models import CraftRecord

# Create your views here.


def craft_overview(request):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context = {}
    context['character'] = character
    context['craft_records'] = CraftRecord.objects.filter(crafter=character)

    return render(request, 'craft/overview.djhtml', context)


def craft_item(request):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context = {}
    context['character'] = character

    if request.method == 'POST':
        form = CraftItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('craft_overview')
        else:
            context['form'] = form
    else:
        context['form'] = CraftItemForm(initial={'crafter': character})
        if sector_controlled_by_corp(character.corporation, 'Ressources naturelles'):
            context['form'].fields['extra_spend'].widget = forms.NumberInput()

    return render(request, 'craft/craft_item.djhtml', context)

def craft_simulator(request):

    character = get_character(request)

    context = {}
    context['character'] = character

    if request.method == 'POST':
        form = CraftSimForm(request.POST)
        if form.is_valid():
            context['form'] = form
            context['resource_list'] = form.resource_list
            return render(request, 'craft/craft_simulator.djhtml', context)
        else:
            context['form'] = form
    else:
        context['form'] = CraftSimForm(initial={'crafter': character})

    return render(request, 'craft/craft_simulator.djhtml', context)