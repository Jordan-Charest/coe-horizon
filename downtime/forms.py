from character.models import Character, SkillBought, Skill
from django.forms import ModelForm, ChoiceField
from django.forms.models import ModelChoiceField, ModelMultipleChoiceField
from downtime.models import Action, Assistance, Sector, Tick
from character.utils import char_has_skill
from django import forms


class CreateActionForm(ModelForm):
    invited_characters = ModelMultipleChoiceField(
        required=False,
        queryset = Character.objects.filter(deactivated=False)
    )
    choices = [choice for choice in Action.TYPES if choice[0] != 'AS']
    action_type = ChoiceField(choices=choices)

    class Meta:
        model = Action
        fields = [
            'action_type',
            'invited_characters',
            'character',
            'tick',
            'is_skill_free_action',
            'free_action_skill_used',
            'approval_required',
            ]
        labels = {
            'action_type': "Type d'Action",
            'invited_characters': 'Personnages invités',
            'is_skill_free_action': 'Action Gratuite (Cochez si vous utilisez une compétence qui vous l\'octroie)',
            'free_action_skill_used': 'Compétence vous octroyant une action gratuite'
        }
        widgets = {
            'character': forms.HiddenInput(),
            'tick': forms.HiddenInput(),
            'approval_required': forms.HiddenInput(),
        }


    def clean(self):
        cleaned_data = super().clean()
        character = cleaned_data.get('character')
        action_type = cleaned_data.get('action_type')
        is_free_action = cleaned_data.get('is_skill_free_action')
        free_action_skill_used = cleaned_data.get('free_action_skill_used')

        free_action_skills = [
            'Relations corporatives',
            'Terrorisme',
            'Alter-Ego Virtuel',
            'Systèmes de sécurité',
        ]
        if is_free_action:
            if free_action_skill_used is None:
                raise forms.ValidationError('Veuillez spécifier la compétence vous octroyant un Downtime gratiuit')

            if free_action_skill_used.name not in free_action_skills and not free_action_skill_used.name.startswith("Relations corporatives"):
                raise forms.ValidationError(f'La compétence {free_action_skill_used.name} ne permets pas de prendre des actions de Downtime gratuites!')

            if not char_has_skill(character, free_action_skill_used.name):
                raise forms.ValidationError(f'Vous n\'avez pas la compétence {free_action_skill_used.name}!')

            if free_action_skill_used.name.startswith('Relations corporatives'):
                if not (action_type == Action.CONQUEST or action_type == Action.DEFENCE):
                    raise forms.ValidationError('Relations corporatives ne peut être utilisé que pour des actions de défense ou conquête!')

            if free_action_skill_used.name == 'Terrorisme':
                if not action_type == Action.SABOTAGE:
                    raise forms.ValidationError('Terrorisme ne peut être utilisé que pour des actions de sabotage!')

            if free_action_skill_used.name == 'Alter-Ego Virtuel':
                self.cleaned_data['approval_required'] = True

            actions = Action.objects.filter(
                character=character,
                tick=Tick.objects.get(completed=False),
                free_action_skill_used=free_action_skill_used
            )

            if len(actions) > 0:
                raise forms.ValidationError(f'Vous ne pouvez utiliser "{free_action_skill_used}" qu\'une seule fois par Downtime!')


        if action_type != Action.ASSISTANCE and character.remaining_downtimes < 1 and not is_free_action:
            raise forms.ValidationError("Vous n'avez pas assez d'actions de downtime pour compléter cette action!")

        if action_type == Action.AUGMENTATION:
            install_skills = [
                'Bioingénierie - Innovation',
                'Cyberpsychologie',
                'Robotique - Innovation',
                'Cybernétique - Innovation',
            ]

            char_can_install = False
            for skill in install_skills:
                if char_has_skill(character, skill):
                    char_can_install = True

            if not char_can_install:
                raise forms.ValidationError('Vous devez avoir une compétence permettant d\'installer une augmentation pour utiliser cette action!')

        if action_type == Action.GENERATION:
            generator_skills = [
                    'Pilotage avancé',
                    'Ingénierie spécialisée - Innovation',
                ]

            char_can_generate = False
            for skill in generator_skills:
                if char_has_skill(character, skill):
                    char_can_generate = True

            if not char_can_generate:
                raise forms.ValidationError('Vous devez avoir une compétence vous permettant de générer de l\'influence pour utiliser cette action!')

    def __init__(self, *args, **kwargs):
        super(CreateActionForm,self).__init__(*args, **kwargs)
        self.fields['invited_characters'].queryset = Character.objects.filter(player__isnull=False)


class SectorActionForm(ModelForm):
    sector = ModelChoiceField(required=True, queryset=Sector.objects.all(), label='Secteur Visé')

    class Meta:
        model = Action
        fields = ['influence_invested',
                  'sector',
                  'reduces_workers_vd',
                  'reduces_military_vd',
                  ]
        labels = {
            'influence_invested': 'Influence Investie',
            'reduces_workers_vd': "Réduire la VD du secteur \"Main d'oeuvre : Ouvriers\" de 1 pour augmenter la défense de 1",
            'reduces_military_vd': "Réduire la VD du secteur Militaire de 1 pour augmenter l'influence du sabotage de 1",
            'sector': 'Secteur Visé'
        }
        widgets = {
            'reduces_workers_vd': forms.HiddenInput(),
            'reduces_military_vd': forms.HiddenInput(),
        }

class BuySellForm(ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        character = cleaned_data.get('character')
        shop_skill = cleaned_data.get('shop_skill')

        shop_skills = [
            'Commerce',
            'Marché noir',
            'Fournisseurs',
            'Fournisseurs d\'équipement de pointe'
        ]

        if shop_skill is None:
            raise forms.ValidationError(f'Vous devez sélectionner une compétence!')

        if shop_skill.name not in shop_skills:
            raise forms.ValidationError(f'{shop_skill} n\'est pas une compétence permettant cette action!')

        if not char_has_skill(character, shop_skill.name):
            raise forms.ValidationError(f'Votre personnage n\'à pas la compétence {shop_skill}!')

    class Meta:
        model = Action
        fields = [
            'operation_description',
            'shop_skill',
            'character',
        ]
        widgets = {
            'character': forms.HiddenInput(),
        }
        labels = {
            'operation_description': 'Objets à vendre ou acheter',
            'shop_skill': 'Compétence utilisée'
        }


class CreateOperationForm(ModelForm):
    class Meta:
        model = Action
        fields = [
            'operation_name',
            'operation_pertinent_stats',
            'operation_objective',
            'operation_description',
            'influence_invested',
            'reduces_military_vd',
            'reduces_workers_vd',
        ]
        labels = {
            'operation_name': "Nom de l'opération",
            'operation_description': 'Description Roleplay',
            'influence_invested': 'Influence Investie',
            'operation_pertinent_stats': 'Statistiques pertinentes à l\'action (Ex: spécialités d\'influence)',
            'operation_objective': 'Objectif de l\'opération',
            'reduces_military_vd': "Réduire la VD du secteur militaire de 1 pour augmenter l'influence d'une opération avec spécialité militaire de 1",
            'reduces_workers_vd': "Réduire la VD du secteur \"Main d'oeuvre : Ouvriers\" de 1 pour augmenter l'influence de l'opération 1",
        }
        widgets = {
            'reduces_military_vd': forms.HiddenInput(),
            'reduces_workers_vd': forms.HiddenInput(),
        }


class AssistanceSectorForm(ModelForm):
    class Meta:
        model = Assistance
        fields = ['influence_invested']
        labels = {
            'influence_invested': "Influence investie"
        }


class AssistanceOperationForm(ModelForm):
    class Meta:
        model = Assistance
        fields = ['influence_invested', 'active_participation']
        labels = {
            'influence_invested': 'Influence à investir',
            'active_participation': 'Participation Active'
        }


class AnimAnswer(ModelForm):
    class Meta:
        model = Action
        fields = ['anim_answer', 'answer_completed']
        labels = {
            'anim_answer': 'Réponse de l\'animation',
            'answer_completed': 'Réponse Complétée',
        }
