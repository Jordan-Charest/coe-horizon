import numpy as np
from django.core.management.base import BaseCommand

from downtime.utils import send_answers


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        send_answers()
