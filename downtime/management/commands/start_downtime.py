import numpy as np
from django.core.management.base import BaseCommand

from character.models import Character
from character.utils import char_has_skill, char_has_skill_icontains, char_has_innovation_skill


def pay_influence_everyone():
    # Get or create pay character
    for character in Character.objects.filter(current_larp_active=True):

        influence_to_add = 0

        origins = [
            'Aristocratie corporative',
            'Une toile de contacts',
        ]

        if character.origin.name in origins:
            influence_to_add += 1

        skills = [
            ('Contact corporatif', 1),
            ('Réseau corporatif', 1),
            ('Relations corporatives', 1),
            ('Alliés clandestins', 1),
        ]

        for skill in skills:
            if char_has_skill_icontains(character, skill[0]):
                influence_to_add += skill[1] * char_has_skill_icontains(character, skill[0])


        character.current_influence += influence_to_add
        character.save()

def pay_innovation_everyone():
    for character in Character.objects.filter(current_larp_active=True):
        if char_has_innovation_skill(character):
            character.passive_innovation += 1
            character.save()


class Command(BaseCommand):
    help = 'Provides Influence Points related to Skills for the last LARP'

    def handle(self, *args, **kwargs):
        pay_influence_everyone()
        pay_innovation_everyone()
