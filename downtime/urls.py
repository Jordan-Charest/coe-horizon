from django.urls import path
from downtime import views

urlpatterns = [
    path('overview/', views.overview, name='downtime_overview'),
    path('create_action/', views.create_action, name='create_action'),
    path('process_sector/<action_id>', views.process_sector, name='process_sector'),
    path('process_operation/<action_id>', views.process_operation, name='process_operation'),
    path('downtime_craft/<action_id>', views.downtime_craft, name='downtime_craft'),
    path('create_assistance/<action_id>', views.create_assistance, name='create_assistance'),
    path('anim_answer/<action_id>', views.anim_answer, name='anim_answer'),
    path('answer_overview/', views.answer_overview, name='answer_overview'),
    path('shop/<action_id>', views.shop, name='shop'),
    path('dt_heal_condition/<action_id>/<condition_id>', views.dt_heal_condition, name='dt_heal_condition'),
    path('dt_select_heal_condition/<action_id>', views.dt_select_heal_condition, name='dt_select_heal_condition'),
]
