from downtime.models import Assistance, Action, Tick, Sector
from character.models import Corporation
from character.utils import get_generic_account
from extra_mail.models import Message
from scipy.stats import norm
import numpy as np


def sector_controlled_by_corp(corporation, sector_name):
    current_sector = Sector.objects.get(name=sector_name)

    if current_sector.controlling_character:
        return corporation == current_sector.controlling_character.corporation
    else:
        return False


def sector_controlled_by_char(character, sector_name):
    current_sector = Sector.objects.get(name=sector_name)

    return character == current_sector.controlling_character


def get_corruption_cost(character):
    player_corps = Corporation.objects.filter(anim_only=False)
    sectors = Sector.objects.all()

    controlled_by_corp = []
    for sector in sectors:
        if sector.controlling_character:
            controlled_by_corp.append(sector.controlling_character.corporation)

    count_by_corp = []
    for corp in player_corps:
        count_by_corp.append(controlled_by_corp.count(corp))

    median, std = norm.fit(count_by_corp)
    x = [median + -std * 3, median + std * 3]
    y = [-0.75, 2]

    coeficients = np.polyfit(x, y, 1)
    curve = np.poly1d(coeficients)

    return  int(500+500*curve(controlled_by_corp.count(character.corporation)))


def get_current_assistances_for_char(character):
    try:
        tick = Tick.objects.get(completed=False)
        requests = Action.objects.filter(
            invited_characters__in=[character],
            tick=tick
        )

        requests = [
            action for action in requests
            if len(Assistance.objects.filter(action=action, character=character)) == 0
        ]
        if len(requests) > 0:
            return requests

    except Tick.DoesNotExist:
        pass

    return None


def send_answers():
    actions = Action.objects.filter(answer_completed=True, answer_sent=False)
    downtime_account = get_generic_account('DOWNTIME')
    for action in actions:
        assistances = Assistance.objects.filter(
            action=action,
            active_participation=True
        )
        message = Message(
            by=downtime_account,
            subject=f'Réponse downtime "{action.operation_name}"',
            content=action.anim_answer.__str__()
        )
        message.save()
        message.to.add(action.character)

        for assistance in assistances:
            message.to.add(assistance.character)
        message.save()

        action.answer_sent = True
        action.save()

