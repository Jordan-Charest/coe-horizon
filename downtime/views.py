from character.models import Character
from character.utils import char_has_skill, get_character, get_generic_account
from django.shortcuts import redirect, render
from django import forms
from downtime.forms import *
from downtime.models import Action, Assistance, Sector, Tick
from downtime.utils import get_current_assistances_for_char, sector_controlled_by_char, sector_controlled_by_corp
from craft.forms import CraftItemForm
from conditions.models import AppliedCondition
from bank_account.models import Transaction

# Create your views here.

def overview(request):
    context = {}
    context['character'] = get_character(request)
    context['sectors'] = Sector.objects.all()

    tick = Tick.objects.filter(completed=False).first()
    if tick:
        context['tick'] = tick

    return render(request, 'downtime/overview.djhtml', context)
    # return redirect('create_action')

def anim_answer(request, action_id):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect('create_action')

    context = {}
    action = Action.objects.get(id=action_id)
    context['action'] = action

    if request.method == 'POST':
        form = AnimAnswer(request.POST)
        if form.is_valid():
            to_update = form.save(commit=False)

            action.anim_answer = to_update.anim_answer
            action.answer_completed = to_update.answer_completed

            action.save()


            return redirect('answer_overview')
        else:
            context['form'] = form
    else:
        context['form'] = AnimAnswer(instance=action)

    return render(request, 'downtime/anim_answer.djhtml', context)

def answer_overview(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect('create_action')

    context = {}
    actions = Action.objects.filter(action_type=Action.OPERATION, answer_completed=False)

    context['actions'] = actions

    return render(request, 'downtime/answer_overview.djhtml', context)

def create_action(request):
    character = get_character(request)
    context = {}
    context['character'] = character
    context['assistance_requests'] = get_current_assistances_for_char(character)

    if request.method == 'POST':
        form = CreateActionForm(request.POST)
        if form.is_valid():
            action = form.save(commit=False)
            action.save()
            action.invited_characters.add(*form.cleaned_data['invited_characters'])
            sector_types = [Action.CONQUEST, Action.DEFENCE, Action.SABOTAGE, Action.SECURITY_SYSTEM]
            if action.action_type in sector_types:
                return redirect('process_sector', action_id=action.id)
            if action.action_type == Action.OPERATION:
                return redirect('process_operation', action_id=action.id)
            if action.action_type == Action.CRAFT:
                return redirect('downtime_craft', action_id=action.id)
            if action.action_type == Action.AUGMENTATION:
                return redirect('create_action')
            if action.action_type == Action.MILITARY_SECTOR:
                return redirect('dt_select_heal_condition', action_id=action.id)
            if action.action_type == Action.GENERATION:
                character.current_influence += 1
                character.save()
                return redirect('create_action')
            if action.action_type == Action.SHOP:
                return redirect('shop', action_id=action.id)
        else:
            context['form'] = form
    else:
        context['form'] = CreateActionForm(initial={
            'character': character,
            'tick': Tick.objects.get(completed=False)
            })

    form = context['form']
    if not sector_controlled_by_corp(character.corporation, "Militaire"):
        action_type_choices = form.fields['action_type'].choices
        new_choices = [choice for choice in action_type_choices if choice[0] != Action.MILITARY_SECTOR]
        form.fields['action_type'].choices = new_choices

    if not char_has_skill(character, "Systèmes de sécurité"):
        action_type_choices = form.fields['action_type'].choices
        new_choices = [choice for choice in action_type_choices if choice[0] != Action.SECURITY_SYSTEM]
        form.fields['action_type'].choices = new_choices

    context['actions'] = Action.objects.filter(character=character, tick=Tick.objects.last())
    return render(request, 'downtime/create_action.djhtml', context)

def shop(request, action_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    context = {}
    context['action'] = action

    if action.character != character:
        return redirect('create_action')

    if request.method == 'POST':
        form = BuySellForm(data=request.POST)

        if form.is_valid():
            action_upd = form.save(commit=False)
            action_upd.operation_name = 'SHOPPING'
            action.save()

            return redirect('create_action')
        else:
            context['form'] = form
    else:
        context['form'] = BuySellForm(initial={'character': character})

    return render(request, 'downtime/buysell.djhtml', context)


def dt_select_heal_condition(request, action_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    context = {}
    context['action'] = action
    context['conditions'] = AppliedCondition.objects.filter(character=character,
                                                             healed_at=None)

    if action.character != character:
        return redirect('create_action')

    return render(request, 'downtime/heal_condition.djhtml', context)

def dt_heal_condition(request, action_id, condition_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    condition = AppliedCondition.objects.get(id=condition_id)

    action.healed_condition = condition
    action.save()

    return redirect('create_action')


def process_sector(request, action_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    context = {}
    context['action'] = action

    if action.character != character:
        return redirect('create_action')

    if request.method == 'POST':
        form = SectorActionForm(request.POST)

        if form.is_valid():
            action_upd = form.save(commit=False)
            action.influence_invested = action_upd.influence_invested
            action.sector = action_upd.sector

            controls_military = sector_controlled_by_char(character, 'Militaire')
            if controls_military:
                action.reduces_military_vd = action_upd.reduces_military_vd
                
            controls_workers = sector_controlled_by_char(character, "Main d'oeuvre : Ouvriers")
            if controls_workers:
                action.reduces_workers_vd = action_upd.reduces_workers_vd

            if action.action_type == Action.SECURITY_SYSTEM:
                fail = False
                if action_upd.influence_invested > 1:
                    context['error'] = "Vous ne pouvez acheter qu'un seul point de VD avec cette compétence"

                if not fail:
                    security_account = get_generic_account('Systèmes de sécurité').bank_account
                    transaction = Transaction(amount=action_upd.influence_invested * 500,
                                              source=character.bank_account,
                                              destination=security_account)
                    action.is_skill_free_action = True
                    
                    try:
                        transaction.save()
                    except Exception as e:
                        context['error'] = e

                    action.save()

                return redirect('downtime_overview')
            elif action.influence_invested <= character.effective_influence:
                action.save()
                return redirect('downtime_overview')
            else:
                context['error'] = "Vous n'avez pas suffisamment d'influence pour compléter cette action."

    form = SectorActionForm(instance=action)
    if sector_controlled_by_char(character, "Main d'oeuvre : Ouvriers") and action.action_type == Action.DEFENCE:
        form.fields['reduces_workers_vd'].widget = forms.CheckboxInput()
    if sector_controlled_by_char(character, 'Militaire') and action.action_type == Action.SABOTAGE:
        form.fields['reduces_military_vd'].widget = forms.CheckboxInput()
    if action.action_type == Action.SECURITY_SYSTEM:
        form.fields['influence_invested'].label = "Influence à acheter"
    context['form'] = form
    context['effective_influence'] = character.effective_influence
    return render(request, 'downtime/process_sector.djhtml', context)


def process_operation(request, action_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    context = {}
    context['action'] = action

    if action.character != character:
        return redirect('create_action')

    if request.method == 'POST':
        form = CreateOperationForm(request.POST)

        if form.is_valid():
            action_upd = form.save(commit=False)
            action.influence_invested = action_upd.influence_invested
            action.operation_name = action_upd.operation_name
            action.operation_description = action_upd.operation_description
            action.operation_objective = action_upd.operation_objective
            action.operation_pertinent_stats = action_upd.operation_pertinent_stats


            controls_military = sector_controlled_by_char(character, 'Militaire')
            controls_workers = sector_controlled_by_char(character, "Main d'oeuvre : Ouvriers")
            if action.reduces_military_vd and not controls_military:
                context['error'] = "Vous ne pouvez pas réduire le VD du secteur militaire sans le controller!"
            elif action.reduces_workers_vd and not controls_workers:
                context['error'] = "Vous ne pouvez pas réduire le VD de la main d'oeuvre ouvrière sans les controller!"
            elif action.influence_invested <= character.effective_influence:
                action.save()
                return redirect('downtime_overview')
            else:
                context['error'] = "Vous n'avez pas suffisamment d'influence pour compléter cette action."


    form = CreateOperationForm(instance=action)
    if sector_controlled_by_char(character, 'Militaire'):
        form.fields['reduces_military_vd'].widget = forms.CheckboxInput()
    if sector_controlled_by_char(character, "Main d'oeuvre : Ouvriers"):
        form.fields['reduces_workers_vd'].widget = forms.CheckboxInput()
    context['form'] = form
    context['effective_influence'] = character.effective_influence
    return render(request, 'downtime/process_operation.djhtml', context)


def downtime_craft(request, action_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    context = {
        'action': action,
        'character': character,
        'post_location': 'downtime_craft',
        'post_location_id': action_id,
    }

    if character != action.character:
        return redirect('create_action')

    if request.method == 'POST':
        form = CraftItemForm(request.POST)
        if form.is_valid():
            craft_record = form.save()
            action.craft_record = craft_record
            action.save()

            if character.loot:
                character.loot = character.loot + f'\n{craft_record.recipe}'
            else:
                character.loot = f'{craft_record.recipe}'

            character.save()

            return redirect('create_action')
        else:
            context['form'] = form
    else:
        context['form'] = CraftItemForm(initial={
            'crafter':character,
            'downtime': True}
        )

    return render(request, 'craft/craft_item.djhtml', context)


def create_assistance(request, action_id):
    character = get_character(request)
    action = Action.objects.get(id=action_id)
    context = {
        'action': action,
        'character': character
    }

    if character not in action.invited_characters.all():
        return redirect('create_action')

    if request.method == 'POST':
        if action.action_type == Action.OPERATION:
            form = AssistanceOperationForm(request.POST)
        else:
            form = AssistanceSectorForm(request.POST)

        if form.is_valid():
            # TODO: check user still have enough downtimes if active participation!
            assistance = form.save(commit=False)
            new_action = Action(
                character=character,
                tick=Tick.objects.get(completed=False),
                action_type=Action.ASSISTANCE
            )
            new_action.save()
            assistance.action = action
            assistance.character = character
            assistance.save()
            new_action.assisting = assistance
            new_action.save()

            return redirect('create_action')
        else:
            context['form'] = form
    else:
        if action.action_type == Action.OPERATION:
            form = AssistanceOperationForm()
        else:
            form = AssistanceSectorForm()

        context['form'] = form

    return render(request, 'downtime/create_assistance.djhtml', context)
