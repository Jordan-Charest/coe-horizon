from django.contrib import admin

from .models import ContactList, Message, MessageStatus

# Register your models here.
admin.site.register(Message)
admin.site.register(ContactList)
admin.site.register(MessageStatus)
