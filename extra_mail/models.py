from django.db import models

from character.models import Character


# Create your models here.
class ContactList(models.Model):
    owner = models.OneToOneField(Character, on_delete=models.CASCADE)
    contacts = models.ManyToManyField(Character, related_name='contacts')

    def __str__(self):
        return self.owner.__str__()

class Message(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    by = models.ForeignKey(Character, on_delete=models.CASCADE, related_name='by')
    to = models.ManyToManyField(Character, related_name='to')
    cci = models.ManyToManyField(Character, related_name='cci', blank=True)
    subject = models.CharField(max_length=200)
    content = models.TextField()
    previous = models.ForeignKey('self', on_delete=models.PROTECT, null=True, default=None)

    def __str__(self):
        return self.subject

class MessageStatus(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    owner = models.ForeignKey(Character, on_delete=models.CASCADE)
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    read = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.message.subject} ({self.owner.__str__()})'
