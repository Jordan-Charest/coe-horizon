from django.urls import path

from . import views

urlpatterns = [
    path('add_contact/', views.add_contact, name='add_contact'),
    path('display_qr/', views.display_qr, name='display_qr'),
    path('send_message/', views.send_message, name='send_message'),
    path('send_message/<from_id>', views.send_message, name='send_message'),
    path('overview/', views.overview, name='message_overview'),
    path('overview/<from_id>', views.overview, name='message_overview_from'),
    path('contacts/', views.contact_list, name='contacts'),
    path('mark_unread/<message_status_id>', views.mark_unread, name='mark_unread'),
    path('mark_unread/<message_status_id>/<from_id>', views.mark_unread, name='mark_unread'),
    path('outbox/', views.outbox, name='outbox'),
    path('view_message/<message_status_id>', views.view_message, name='view_message'),
    path('reply_message/<message_id>', views.reply, name='reply_message'),
    path('replyall_message/<message_id>', views.reply_all, name='replyall_message'),
    path('forward_message/<message_id>', views.forward, name='forward_message'),
    path('view_message/<message_status_id>/<from_id>', views.view_message, name='view_message_from'),
    path('reply_message/<message_id>/<from_id>', views.reply, name='reply_message_from'),
    path('replyall_message/<message_id>/<from_id>', views.reply_all, name='replyall_message_from'),
    path('forward_message/<message_id>/<from_id>', views.forward, name='forward_message_from'),
]
