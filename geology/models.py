from collections import Counter

from django.db import models
from django.utils import timezone

from character.models import Character
from larpdate.utils import get_larp_at_datetime


# Create your models here.
class Mineral(models.Model):
    name = models.CharField(max_length=100)
    market_price = models.PositiveIntegerField()
    chance_to_spawn = models.DecimalField(decimal_places=2, max_digits=5)

    def __str__(self):
        return self.name

class Pole(models.Model):
    security_token = models.CharField(max_length=70, null=True, blank=True)
    owner = models.ForeignKey(Character, on_delete=models.CASCADE, null=True, blank=True)

    completed = models.BooleanField(default=False)
    started = models.BooleanField(default=False)
    installed_at = models.DateTimeField(blank=True, null=True)
    stopped_at = models.DateTimeField(blank=True, null=True)

    collected = models.BooleanField(default=False)

    target_mineral = models.ForeignKey(Mineral, on_delete=models.CASCADE)
    output = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.owner} mine {self.target_mineral.name} @ {self.installed_at}'

    def collect(self):
        from geology.util import generate_pole_results

        account = self.owner.bank_account
        if True:
        # if account.balance >= 50 and not self.collected:
            # account.balance = account.balance - 50
            # account.save()

            if not self.completed:
                self.complete()

            time_elapsed = (self.stopped_at - self.installed_at).total_seconds() / 3600
            credit_value = min(time_elapsed * 60 * 2, 500)
            self.collected = True
            self.save()

            result = generate_pole_results(credit_value, self.target_mineral)

            str_list = [i.__str__() for i in result]
            counter = Counter(str_list)
            output = ''
            for i in counter.items():
                output = output + i[0] + ': ' + i[1].__str__() + '\n'

            self.output = output
            self.save()

            return result
        else:
            return None

    def complete(self):
        self.completed = True
        now = timezone.now()
        larp = get_larp_at_datetime(self.installed_at)
        if now > larp.end_datetime:
            self.stopped_at = larp.end_datetime
        else:
            self.stopped_at = timezone.now()

        self.save()

