from django.db import models

from character.models import Character
from extra_mail.models import Message

# Create your models here.

class HackAttempt(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    # Characters
    hacker = models.ForeignKey(Character, related_name='hacker', on_delete=models.CASCADE)
    target = models.ForeignKey(Character, related_name='target', on_delete=models.CASCADE)

    # Chances
    detection_chance = models.DecimalField(max_digits=5, decimal_places=2)
    roll = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    success= models.BooleanField(null=True)

    # Status
    processed = models.BooleanField(default=False)

    # Results
    stolen_amount = models.IntegerField(default=0)

    # emails
    email_to_steal = models.IntegerField(default=0)
    stolen_mails = models.ManyToManyField(Message, related_name='stolen_mail', blank=True )

    # Logs
    programs_applied = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.hacker} => {self.target} @ {self.timestamp}'

class HackProgress(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    # Characters
    hacker = models.ForeignKey(Character, related_name='hackprogress_hacker', on_delete=models.CASCADE)
    target = models.ForeignKey(Character, related_name='hackprogress_target', on_delete=models.CASCADE)

    # Status
    processed = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    processed_at = models.DateTimeField(null=True, blank=True)
    extra_stolen_things = models.IntegerField(default=0)

    hack_attempt = models.ForeignKey(HackAttempt, related_name='hack_attempt', on_delete=models.CASCADE, blank=True, null=True)

    # Points
    # +1 si le pirate a reçu minimum 50 crédits de la cible.
    received_creds = models.BooleanField(default=False)
    # +1 si le pirate a envoyé minimum 50 crédits à la cible.
    sent_creds = models.BooleanField(default=False)
    # +1 si le pirate a initié une conversion courriel en envoyant un courriel à la cible et si la cible a envoyé une réponse par la suite.
    started_convo = models.BooleanField(default=False)
    # +1 si la cible a initié une conversation courriel en envoyant un courriel au pirate et si le pirate a envoyé une réponse par la suite.
    talked_to = models.BooleanField(default=False)
    # +1 si la cible et le pirate ont échangé plus de 5 courriels, sans autres personnes en CC.
    ongoing_pm = models.BooleanField(default=False)
    # +1 si le pirate a installé un programme actif sur la cible. La source du programme (fabriqué ou non par le pirate) n’est pas importante.
    installed_software = models.BooleanField(default=False)

    #MANUAL INTERVENTION BY ANIM
    # +1 si le pirate utilise une identification de vulnérabilité temporaire associée à la cible.
    temp_vuln_used = models.BooleanField(default=False)
    # +1 si le pirate a fait une analyse de la cible avec une utilisation de la compétence Informatique et Cryptographie.
    manual_analysis = models.BooleanField(default=False)


    def __str__(self):
        result = ''
        if self.processed:
            result += '(Complété) '
        else:
            result += '(En cours) '

        result += f'{self.hacker} => {self.target}'

        return result

    @property
    def progress_count(self):
        count = 0

        if self.received_creds : count += 1
        if self.sent_creds : count += 1
        if self.started_convo : count += 1
        if self.talked_to : count += 1
        if self.ongoing_pm : count += 1
        if self.installed_software : count += 1
        if self.temp_vuln_used : count += 1
        if self.manual_analysis : count += 1

        return count
