from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from datetime import timedelta

from bank_account.utils import get_account_owner
from bank_account.models import Transaction
from character.utils import char_has_skill
from extra_mail.models import Message, MessageStatus
from program.models import InstalledProgram

from .models import HackProgress
from .util import get_hacker_progress

@receiver(post_save, sender=Transaction)
def progress_received_creds(sender, instance, created, raw, using, update_fields, **kwargs):
  if instance.amount >= 50:
    hacker = get_account_owner(instance.destination)
    target = get_account_owner(instance.source)
  
    hack_progress = get_hacker_progress(hacker, target)
    if hack_progress:
      hack_progress.received_creds = True
      hack_progress.save()

@receiver(post_save, sender=Transaction)
def progress_sent_creds(sender, instance, created, raw, using, update_fields, **kwargs):
  if instance.amount >= 50:
    hacker = get_account_owner(instance.source)
    target = get_account_owner(instance.destination)
  
    hack_progress = get_hacker_progress(hacker, target)
    if hack_progress:
      hack_progress.sent_creds = True
      hack_progress.save()

@receiver(post_save, sender=MessageStatus)
def progress_started_convo_read(sender, instance, created, raw, using, update_fields, **kwargs):
  if update_fields and 'read' in update_fields and instance.message.previous is None:
    hacker = instance.message.by
    target = instance.owner
    hack_progress = get_hacker_progress(hacker, target)
    if hack_progress and instance.timestamp: 
      hack_progress.started_convo = True
      hack_progress.save()


@receiver(post_save, sender=Message)
def progress_message_ensure_exists(sender, instance, **kwargs):
  by = instance.by
  for dest in instance.to.all():
    get_hacker_progress(by, dest)
    get_hacker_progress(dest, by)

@receiver(post_save, sender=Message)
def progress_talked_to(sender, instance, created, raw, using, update_fields, **kwargs):
  if instance.previous is not None and instance.previous.previous is None:
    target = instance.previous.by
    hacker = instance.by
    if hacker in instance.previous.to.all():
      hack_progress = get_hacker_progress(hacker, target)
      if hack_progress:
        delta = hack_progress.timestamp - instance.previous.timestamp
        if delta < timedelta(seconds=10):
          hack_progress.talked_to = True
          hack_progress.save()

@receiver(post_save, sender=InstalledProgram)
def progress_installed_software(sender, instance, created, raw, using, update_fields, **kwargs):
  hacker = instance.installed_by
  target = instance.target
  hack_progress = get_hacker_progress(hacker, target)
  if hack_progress:
    hack_progress.installed_software = True
    hack_progress.save()

@receiver(post_save, sender=Message)
def progress_ongoing_dm(sender, instance, created, raw, using, update_fields, **kwargs):
  for dest in instance.to.all():
    check_ongoing_dm(instance, instance.by, dest)
    check_ongoing_dm(instance, dest, instance.by)

    
def check_ongoing_dm(message, hacker, target):
  message_count = 0
  hacking_success = True
  if hacker and target:
    hack_progress = get_hacker_progress(hacker, target)
  else:
    return None

  try:
    if hack_progress:
      while message and message_count < 6 and hacking_success:
        message_count += 1

        if not((hacker == message.by or hacker in message.to.all()) and
               (target == message.by or target in message.to.all())):
          hacking_success = False
        if message.to.count() > 1:
          hacking_success = False
          
        message = message.previous

      if message_count > 5 and hacking_success:
        hack_progress.ongoing_pm = True
        hack_progress.save()
  except:
    pass


  
