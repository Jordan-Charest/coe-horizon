import logging
import os
from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler

from hacking.util import process_encrypts


def start():
    scheduler = BackgroundScheduler()
    if 'DJANGO_DEBUG_FALSE' in os.environ and 'SSH_CLIENT' not in os.environ:
        # scheduler.add_job(process_hacks, 'interval', seconds=10)
        scheduler.add_job(process_encrypts, 'interval', seconds=10)
    else:
        # scheduler.add_job(process_hacks, 'interval', minutes=10)
        scheduler.add_job(process_encrypts, 'interval', minutes=10)

    logging.getLogger('apscheduler.executors.default').setLevel(logging.WARNING)
    scheduler.start()
