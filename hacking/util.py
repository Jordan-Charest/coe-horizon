import random
from datetime import date, datetime, timedelta

import pytz
from astral import Astral
from django.utils import timezone

from character.models import Character
from character.utils import char_has_skill, get_generic_account
from hacking.models import HackAttempt, HackProgress
from hacking.exceptions import AccountCompromised, HackedToday
from program import programs
from program.models import InstalledProgram, ProgramType
from extra_mail.models import Message


def get_program(program_id):
    return getattr(programs, program_id)

def process_encrypts():
    encrypted_chars = Character.objects.filter(encrypted=True)
    now = timezone.now()

    for character in encrypted_chars:
        if character.encrypted_until < now:
            character.encrypted = False
            character.save()

def prepare_hack(hacker, target, detection_chance, hacked_amount, stealing='credits'):
    attempt = HackAttempt(
        hacker=hacker,
        target=target,
        detection_chance=detection_chance,
    )

    # Check if the hacker has VC program installed
    vc = ProgramType.objects.get(id_code='VC')
    vc_installed = InstalledProgram.objects.filter(target=hacker, program=vc)
    count_vc_installed = vc_installed.count()

    # Check if hacker hacked target today
    today_min = datetime.combine(date.today(), datetime.min.time())
    hacks_today = HackAttempt.objects.filter(processed=True, target=target, hacker=hacker,timestamp__gte=today_min)
    if hacks_today:
        if count_vc_installed > 0:
            attempt.programs_applied = 'VC, '
            latest_attempt = HackAttempt.objects.filter(
                hacker=hacker,
                target=target
            ).order_by('-id').first()
            attempt.detection_chance = attempt.detection_chance + hacks_today.count() * 0.05
            vc_installed.first().delete()
        else:
            raise HackedToday

    # Run offenssive programs
    hacker_programs = hacker.programs_installed.all()
    for prog in hacker_programs:
        if prog.program.classification == ProgramType.HACK_OFFENCE:
            to_execute = get_program(prog.program.id_code)
            to_execute(attempt)
            attempt.programs_applied = attempt.programs_applied + prog.program.id_code + ', '

    # Run defenssive programs
    target_programs = hacker.programs_installed.all()
    for prog in target_programs:
        if prog.program.classification == ProgramType.HACK_DEFENCE:
            try:
                to_execute = get_program(prog.program.id_code)
                to_execute(attempt)
                attempt.programs_applied = attempt.programs_applied + prog.program.id_code + ', '
            except:
                pass

    attempt.roll = random.uniform(0,1)

    attempt.success = attempt.roll > attempt.detection_chance
    attempt.processed = True

    if attempt.success and stealing=='credits':
        hacker_account = attempt.hacker.bank_account
        target_account = attempt.target.bank_account

        balance = attempt.target.bank_account.balance
        attempt.stolen_amount = round(balance*0.1)

        target_account.balance = target_account.balance - attempt.stolen_amount
        hacker_account.balance = hacker_account.balance + attempt.stolen_amount

        target_account.save()
        hacker_account.save()
    if attempt.success and stealing=='emails':
        attempt.email_to_steal = hacked_amount
        attempt.processed = False

    attempt.save()

    if attempt.success:
        progress = get_hacker_progress(attempt.hacker, attempt.target)
        progress.processed = True
        progress.save()

        get_hacker_progress(attempt.hacker, attempt.target, True)

    return attempt

def get_current_cycle_start():
    a = Astral()
    city_name = 'Montreal'

    city = a[city_name]
    now = datetime.utcnow().replace(tzinfo=pytz.utc)
    yesterday = now - timedelta(hours=24)

    sun_today = city.sun(date=now, local=True)
    sun_yesterday = city.sun(date=yesterday, local=True)

    # If we're before sunrise, sunset of yesterday is start of cycle
    if now < sun_today['sunrise']:
        return sun_yesterday['sunset']
    elif now < sun_today['sunset']:
        return sun_today['sunrise']
    else:
        return sun_today['sunset']

def get_hacker_progress(hacker, target, temp_vuln=False):
  if char_has_skill(hacker, "Piratage simple"):
    hackprogress = HackProgress.objects.filter(
      hacker=hacker,
      target=target,
      processed=False,
    )
    if hackprogress:
      hackprogress = hackprogress.first()
    else:
      hackprogress = HackProgress(hacker=hacker, target=target, temp_vuln_used=temp_vuln)
      hackprogress.save()

    return hackprogress
  else:
    return False


def get_stealable_mails(character):
    mail_attempts = HackAttempt.objects.filter(
        hacker=character,
        success=True,
        processed=False,
        email_to_steal__gt=0
    )
    stealable_mail = []
    generic_accounts = [
        get_generic_account('Piratage'),
        get_generic_account('Paye'),
        get_generic_account('DOWNTIME'),
        get_generic_account("HORS_JEU_ADMIN"),
        get_generic_account('Sonde Biomasse')
    ]
    for attempt in mail_attempts.all():
        iattempt = {
            'id': attempt.id,
            'mails': []
        }
        target = attempt.target
        iattempt['mails'] += list(Message.objects.filter(
            to__in=[attempt.target],
            timestamp__lte=attempt.timestamp,
        ).exclude(by__in=generic_accounts))
        iattempt['mails'] += list(Message.objects.filter(
            cci__in=[attempt.target],
            timestamp__lte=attempt.timestamp,
        ).exclude(by__in=generic_accounts))
        stealable_mail.append(iattempt)

    return stealable_mail
