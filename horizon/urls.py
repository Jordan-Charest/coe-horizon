"""horizon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from character.views import view_login
from django.contrib import admin
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns
from django.urls import include, path

from . import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('payments/', include('payments.urls')),
    path('character/', include('character.urls')),
    path('bank_account/', include('bank_account.urls')),
    path('inventory/', include('inventory.urls')),
    path('extra_mail/', include('extra_mail.urls')),
    path('hacking/', include('hacking.urls')),
    path('geology/', include('geology.urls')),
    path('biomass/', include('biomass.urls')),
    path('craft/', include('craft.urls')),
    path('innovation/', include('innovation.urls')),
    path('anim_tools/', include('anim_tools.urls')),
    path('downtime/', include('downtime.urls')),
    path('conditions/', include('conditions.urls')),
    path('larpdate/', include('larpdate.urls')),
    path('', view_login, name='base_login'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
