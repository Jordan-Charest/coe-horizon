from django.apps import AppConfig


class InnovationConfig(AppConfig):
    name = 'innovation'

    def ready(self):
        import innovation.signals
