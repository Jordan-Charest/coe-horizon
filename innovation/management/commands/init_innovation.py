from django.core.management.base import BaseCommand

from character.models import Character
from character.utils import get_generic_account, char_has_innovation_skill
from bank_account.models import Transaction


def run():
    paye = get_generic_account('Paye')
    for transaction in Transaction.objects.filter(source=paye.bank_account):
        character = transaction.destination.character
        if char_has_innovation_skill(character) and character.passive_innovation < 2:
            character.passive_innovation += 1
            character.save()

    paye = Character.objects.get(
        first_name='Automatique:', last_name='Service de la paie')
    for transaction in Transaction.objects.filter(source=paye.bank_account):
        try:
            # Some character where deleted without their bank account being cleaned-up...
            character = transaction.destination.character
            if char_has_innovation_skill(character) and character.passive_innovation < 2:
                character.passive_innovation += 1
                character.save()
        except Exception:
            pass


class Command(BaseCommand):
    help = 'Parses Horizon website and puts result in skills'

    def handle(self, *args, **kwargs):
        run()
        # update_contact_skills()
