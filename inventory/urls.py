from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('list_inventory/', views.list_inventory, name='list_inventory'),
]
