from django import forms


class GetUrlForm(forms.Form):
    url = forms.IntegerField(widget=forms.widgets.HiddenInput)
