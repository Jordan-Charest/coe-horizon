from django.test import TestCase
from larpdate.models import LarpDate
from datetime import datetime
from larpdate.templatetags.date_tags import in_date

# Create your tests here.
class  LarpDateTestCase(TestCase):
    def setUp(self):
        self.first_larp = LarpDate(
            out_date=datetime(2019,8,24),
            in_date=datetime(2315,8,31),
            current=False
        )
        self.first_larp.save()

        self.second_larp = LarpDate(
            out_date=datetime(2023, 1, 22),
            in_date=datetime(2315, 9, 30)
        )
        self.second_larp.save()
        
    def test_larp_date_translation(self):
        in_date_first = in_date(self.first_larp.out_date)
        self.assertEqual(in_date_first, '2315-08-31 00:00:00')

        in_date_second = in_date(self.second_larp.out_date)
        self.assertEqual(in_date_second, '2315-09-30 00:00:00')
