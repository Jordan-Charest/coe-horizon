import re
from requests_html import HTMLSession
import requests
import json

from .models import LarpDate
from character.models import Corporation
from datetime import datetime, time, timedelta


def get_larp_at_datetime(datetime):
    return LarpDate.objects.filter(out_date__gte=datetime - timedelta(days=4)).order_by('out_date').first()


def get_next_larp():
    today = datetime.now()

    return LarpDate.objects.filter(out_date__gte=today.strftime('%Y-%m-%d')).order_by('out_date').first()


def larp_is_active():
    ld = LarpDate.objects.filter(current=True).first()

    if ld:
        now = datetime.now()
        if ld.out_date == now.date():
            larp_start = time(13)
            larp_end = time(21)

            if now.time() > larp_start and now.time() < larp_end:
                return True

    return False


def get_link_corp_and_date(link):
    result = query_zeffy(link).split(' ')
 
    values = {
        "corp": Corporation.objects.filter(name__icontains=result[1]).first(),
       }

    if result[2] == 'Saison':
        values['date'] = int(result[3])
        values['season'] = True
    else:
        values['date'] = datetime.strptime(result[2], '%Y-%m-%d').date()
        values['season'] = False
 
    return values

def query_zeffy(ticket_id):
    data = {f"operationName":"scanTicket","variables":{"ticketId": ticket_id },"query":"mutation scanTicket($ticketId: String!) {  scanTicket(ticketId: $ticketId) {    id    scanDates    cancelled    order {      id      locale      lastName      firstName      email      __typename    }    rate {      id      rateFields {        id        locale        title        __typename      }      __typename    }    answers {      ...AnswerFragment      __typename    }    __typename  }}fragment AnswerFragment on AnswerObject {  id  questionId  answer  ticketId  choices {    id    choiceFields {      id      locale      body      __typename    }    __typename  }  question {    id    sortIndex    type    rateQuestions {      id      rateId      __typename    }    questionFields {      id      locale      body      __typename    }    __typename  }  __typename}"}
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0'}
    url = "https://api.zeffy.com/graphql"

    response = requests.post(url, json=data, headers=headers)

    return json.loads(response.text)['data']['scanTicket']['rate']['rateFields'][0]['title']
