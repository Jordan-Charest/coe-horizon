# Generated by Django 2.0 on 2018-02-06 02:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0005_auto_20180205_2119'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='entry_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 2, 5, 21, 32, 22, 751192)),
        ),
    ]
